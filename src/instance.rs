use std::sync::Arc;


pub trait Instance {
    fn instance() -> Arc<Self>;
}