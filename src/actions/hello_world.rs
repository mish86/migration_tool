#[allow(unused_imports)]
use log::{error, warn, info, debug, trace};
use async_trait::async_trait;
use super::{MigrationData, Step, Action};

#[derive(Clone)]
pub struct HelloWorldStep {
    pub name: String,
    pub greeting: String,
}

#[async_trait]
impl Action for HelloWorldStep {
    async fn run(self: Box<Self>, data: MigrationData) -> anyhow::Result<MigrationData> {
        use tokio::time;

        time::sleep(time::Duration::new(2, 0)).await;

        Ok(data)
    }
}

impl Step for HelloWorldStep {
    fn name(&self) -> &'static str { "HelloWorld" }

    fn action(&self) -> Box<dyn Action> {
        Box::new(self.clone())
    }
}