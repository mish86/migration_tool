#[allow(unused_imports)]
use log::{error, warn, info, debug, trace};
use std::collections::HashMap;
use std::sync::{Arc, RwLock, RwLockReadGuard, RwLockWriteGuard};

type Store<T> = HashMap<String, T>;

pub struct StoreContainer<T>(Arc<RwLock<Store<T>>>);


#[allow(dead_code)]
impl<T> StoreContainer<T>
where T: Clone {

    pub (super) fn new() -> Arc<Self> {
        Arc::new(StoreContainer ({
            Arc::new(RwLock::new(HashMap::new()))
        }))
    }

    pub (super) fn with_capacity(capacity: usize) -> Arc<Self> {
        Arc::new(StoreContainer ({
            Arc::new(RwLock::new(HashMap::with_capacity(capacity)))
        }))
    }

    /// Acquires a store for read.
    fn store(&self) -> anyhow::Result<RwLockReadGuard<'_, Store<T>>> {
        self.0.read()
            .map_err(|error| {
                anyhow::Error::msg(error.to_string())
            })
    }

    /// Acquires a store for write.
    fn store_mut(&self) -> anyhow::Result<RwLockWriteGuard<'_, Store<T>>> {
        self.0.write()
            .map_err(|error| {
                anyhow::Error::msg(error.to_string())
            })
    }

    pub fn find(&self, key: &str) -> Option<T> {
        self.store()
            .ok()
            .and_then(|store| store.get(key).cloned())
    }

    pub fn save<'r>(&self, key: &str, item: &'r T) -> Option<&'r T> {
        self.store_mut()
            .ok()
            .and_then(|mut store| {
                store.insert(key.to_string(), item.clone());
                Some(item)
            })
    }

    pub fn remove(&self, key: &str) -> Option<T> {
        self.store_mut()
            .ok()
            .and_then(|mut store| {
                store.remove(key)
                    .map_or_else(|| {
                        None
                    }, |session| {
                        Some(session)
                    })
            })
    }

    pub fn clear(&self) -> anyhow::Result<()> {
        self.store_mut()
            .and_then(|mut store| {
                store.clear();
                Ok(())
            })
    }
}

