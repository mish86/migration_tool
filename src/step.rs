#[allow(unused_imports)]
use log::{error, warn, info, debug, trace};
use async_trait::async_trait;

use super::data::MigrationData;

pub trait Step: Sync + Send {
    fn name(&self) -> &'static str;
    fn action(&self) -> Box<dyn Action>;
}

#[async_trait]
pub trait Action: Send {
    async fn run(self: Box<Self>, data: MigrationData) -> anyhow::Result<MigrationData>;
}