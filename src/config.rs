#[allow(unused_imports)]
use log::{error, warn, info, debug, trace};
use lazy_static::*;
use std::sync::Arc;
use serde::Deserialize;
pub use super::instance::Instance;
use super::APP_CONFIG_FILE;

lazy_static! {
    static ref APP_CONFIG: Arc<AppConfig> = { 
        let config = AppConfig::load(APP_CONFIG_FILE);
        let config = config.map_err(|error| {
            println!("Failed to load app config due: {}", error);
            error
        });

        Arc::new(config.unwrap())
    };
}

#[derive(Debug, Deserialize)]
pub struct AppConfig {
    pub data_path: String,
    pub log_config: String,
    pub skip_steps: Vec<String>,
}

impl AppConfig {
    fn load(path: &str) -> anyhow::Result<Self> {
        let config = std::fs::read_to_string(path)?;
        let config = toml::from_str(&config)?;
        Ok(config)
    }
}

impl Instance for AppConfig {
    fn instance() -> Arc<Self> {
        Arc::clone(&*APP_CONFIG)
    }
}
