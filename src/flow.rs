#[allow(unused_imports)]
use log::{error, warn, info, debug, trace};

use super::step::Step;
use super::data::{MigrationData, Status};
use super::config::*;

pub struct Flow {
    pub name: String,
    pub steps: Vec<Box<dyn Step>>,
}

impl Flow {
    pub async fn start(&self, mut data: MigrationData) -> anyhow::Result<()> {
        data.status = Status::InProgress;

        for step in &self.steps {
            let flow_name = self.name.clone();
            let step_name = step.name().to_string();

            let is_skip = AppConfig::instance().skip_steps.iter()
                .any(|skip_step| &step_name == skip_step);
            if is_skip {
                warn!("Flow {} - Step {} is marked to skip in app config", self.name, step.name());
                continue;
            }

            let action = step.action();

            data = tokio::task::spawn(async move {
                let now = std::time::SystemTime::now();
                let time: chrono::DateTime<chrono::Utc> = now.into();
                let time = chrono::DateTime::format(&time, "%T");
                info!("Flow {} - Step {} - Start - {}", flow_name, step_name, time);

                let data = action.run(data).await;

                now.elapsed().map(|elapsed| {
                    let time: chrono::DateTime<chrono::Utc> = now.into();
                    let time = chrono::DateTime::format(&time, "%T");
                    info!("Flow {} - Step {} - Completed - {} - elapsed({} sec)", flow_name, step_name, time, elapsed.as_secs());
                }).ok();

                data
            }).await
                .map_err(|error| {
                    error!("Flow {} - Step {} is failed due: {}", self.name, step.name(), error);
                    error
                })?
                .map_err(|error| {
                    error!("Flow {} - Step {} is failed due: {}", self.name, step.name(), error);
                    error
                })?;

            data.store(step);
        }

        data.status = Status::Completed;
        info!("Flow {} - {:?}", self.name, data.status);
        
        Ok(())
    }
}