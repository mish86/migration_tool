mod config;
mod actions;
mod step;
mod flow;
mod data;
mod store;
mod instance;

#[allow(unused_imports)]
use log::{error, warn, info, debug, trace};
use config::{AppConfig, Instance};

const APP_CONFIG_FILE: &str = "config.toml";
pub (self) const INPUT_FILE: &str = "data/input.json";

#[tokio::main(worker_threads=2)]
async fn main() -> anyhow::Result<()> {
    log4rs::init_file(AppConfig::instance().log_config.clone(), Default::default())?;

    let input = data::input::Input::load(INPUT_FILE)?;
    let results = futures::future::join_all(input.schedule()).await;

    for result in results {
        match result {
            Ok(result) => {
                match result {
                    Ok(_) => (),
                    Err(error) => {
                        error!("Failed to execute flow {} due: {}", "TODO", error);
                    }
                }
            },
            Err(error) => {
                error!("Failed to schedule flows due: {}", error);
            }
        }
    }

    Ok(())
}
