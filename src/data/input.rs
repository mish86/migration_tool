#[allow(unused_imports)]
use log::{error, warn, info, debug, trace};
use serde::{Serialize, Deserialize};
use super::super::step::Step;

#[derive(Serialize, Deserialize)]
pub struct Input {
    pub data: Vec<MigrationData>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum Status {
    NotStarted,
    InProgress,
    Completed,
}

#[derive(Clone, Serialize, Deserialize)]
pub struct MigrationData {
    #[serde(default = "default_status")]
    pub status: Status,
    #[serde(default)]
    pub completed_steps: Vec<String>,
    pub last_error: Option<String>,
    pub hsi_number: String,
}

fn default_status() -> Status {
    Status::NotStarted
}

impl Input {
    pub fn load(path: &str) -> anyhow::Result<Self> {
        let file = std::fs::File::open(path)?;
        let reader = std::io::BufReader::new(file);
        let input = serde_json::from_reader(reader)?;

        Ok(input)
    }

    pub fn schedule(&self) -> Vec<tokio::task::JoinHandle<anyhow::Result<()>>> {

        use super::{StoreContainer, Instance};

        let store: std::sync::Arc<StoreContainer<MigrationData>> = StoreContainer::<MigrationData>::instance();

        self.data.iter()
            .map(|data: &MigrationData| {
                let key = data.hsi_number.clone();
                store.save(&key, data);

                let mut steps: Vec<Box<dyn super::super::step::Step>> = Vec::with_capacity(10);
                for step_id in 1..10 {
                    use super::super::actions::hello_world::HelloWorldStep as Step1;
            
                    steps.push(Box::new(Step1 {
                        name: format!("Flow {} - Step {}", key, step_id),
                        greeting: format!("Hello world - {}", step_id),
                    }));
                }
                let flow = super::super::flow::Flow { name: key, steps };
                let data = data.clone();
        
                tokio::task::spawn(async move { 
                    flow.start(data).await 
                })
            }).collect()
    }
}

impl MigrationData {
    pub fn store(&mut self, completed_step: &Box<dyn Step>) {
        self.completed_steps.push(completed_step.name().to_string());

        // info!("Step {} - COMPLETED", completed_step.name());
    }
}