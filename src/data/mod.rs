pub mod input;

pub use input::{MigrationData, Status};

use lazy_static::*;
use std::sync::Arc;
pub use super::instance::Instance;
use super::store::StoreContainer;

lazy_static! {
    static ref MIGRATION_DATA: Arc<StoreContainer<MigrationData>> = StoreContainer::with_capacity(100);
}

impl Instance for StoreContainer<MigrationData> {
    fn instance() -> Arc<Self> {
        Arc::clone(&*MIGRATION_DATA)
    }
}

